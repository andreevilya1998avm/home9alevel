<?php
echo '<div style="margin-left: 60px">';
echo '<p>*Task 1:</p>';
$num1 = 42;
$num2 = 55;
echo $num1 > $num2 ? '<pre>Numbe'. $num1. 'is more than number $num2</pre>' : '<pre>Number'. $num2. 'is more than number'. $num1. '</pre>';

echo '<p>*Task 2:</p>';
$num1 = rand(5, 15);
$num2 = rand(5, 15);
echo $num1 > $num2 ? '<pre>Number'. $num1. 'is more than number $num2</pre>' : '<pre>Number'. $num2. 'is more or equal than number $num1</pre>';

echo '<p>*Task 3:</p>';
$name = 'Ilya';
$surname = 'Andreev';
$patronimic = 'Vyacheslavovich';
echo '<pre>My abbrevieted full name: '. $surname . ' '. $name[0] . ' '. $patronimic[0] . '.</pre>';

echo '<p>*Task 4:</p>';
$num1 = rand(1, 99999);
$num2 = 7;
echo '<pre>Number $num2 in the number $num1 occurs '. substr_count($num1, $num2). ' time</pre>';

echo '<p>*Task 5:</p>';
$num = 3;
echo '<pre>a) Number a = $num;</pre>';
$num1 = 10;
$num2 = 2;
echo '<pre>b) $num1 + $num2 = '. ($num1 + $num2). ',';
echo '<br>   $num1 - $num2 = '. ($num1 - $num2). ',';
echo '<br>   $num1 * $num2 = '. ($num1 * $num2). ',';
echo '<br>   $num1 / $num2 = '. ($num1 / $num2). ';</pre>';
$num1 = 15;
$num2 = 10;
$result = $num1 + $num2;
echo '<pre>c) $num1 + $num2 = $result;</pre>';
$num1 = 10;
$num2 = 2;
$num3 = 5;
echo '<pre>d) $num1 + $num2 + $num3 = '. ($num1 + $num2 + $num3). ';</pre>';
$num1 = 17;
$num2 = 10;
$num4 = $num3 = $num1 + $num2;

echo '<p>*Task 6:</p>';
$result = $num4 + $num3;
echo '<pre>$num4 + $num3 = $result</pre>';

echo '<p>*Task 7:</p>';
$text = 'Hello wolrd!';
echo '<pre>a) $text;</pre>';
$text1 = 'Hello ';
$text2 = 'world!';
echo '<pre>b) '. $text1. $text2. ';</pre>';
$hourInSec = 60 * 60;
$dayInSec = 24 * $hourInSec;
$weekInSec = 7 * $dayInSec;
$monthInSec = 30 * $dayInSec;
echo '<pre>с) 1 hour = $hourInSec seconds,';
echo '<br>   1 day = $dayInSec seconds,';
echo '<br>   1 week = $weekInSec seconds,';
echo '<br>   1 month = $monthInSec seconds;</pre>';

echo '<p>*Task 8:</p>';
$num = 1;
$num += 12;
$num -= 14;
$num *= 5;
$num /= 7;
$num++;
$num--;
echo '<pre>$num</pre>';
echo '</div>';

echo '<div style="margin-left: 120px;">';
echo '<p>**Task 1:</p>';
$second = date('s');
$minute = date('i');
$hour = date('h') + 3;
echo '<pre>The current time: $hour:$minute:$second</pre>';

echo '<p>**Task 2:</p>';
$text = 'I';
$text .= ' want';
$text .= ' to know';
$text .= ' PHP!';
echo '<pre>$text</pre>';

echo '<p>**Task 3:</p>';
$foo = 'bar';
$bar = 10;
echo '<pre>'. $$foo. '</pre>';

echo '<p>**Task 4:</p>';
$num1 = 2;
$num2 = 4;
echo '<pre>'. (($num1++) + $num2). '</pre>';
echo '<pre>'. ($num1 + ++$num2). '</pre>';
echo '<pre>'. ((++$num1) + $num2++). '</pre>';

echo '<p>**Task 5:</p>';
echo isset($issdsdnum) ? '<pre>a) The variable exists;</pre>' : '<pre>a) The variable does not exist;</pre>';
$text = 'abs';
echo gettype($text) == 'double' ? '<pre>b) The variable is of type double;</pre>' : '<pre>a) The variable is of type not a double;</pre>';
$text = null;
echo is_null($text) ? '<pre>c) The variable is null;</pre>' : '<pre>c) The variable is not null;</pre>';
$text = '';
echo empty($text) ? '<pre>d) The variable is empty;</pre>' : '<pre>d) The variable is not empty;</pre>';
$num = 36;
echo is_integer($num) ? '<pre>e) The variable is an integer;</pre>' : '<pre>e) The variable is not an integer;</pre>';
$num = 36.6;
echo is_double($num) ? '<pre>f) The variable is a fractional number;</pre>' : '<pre>f) The variable is not a fractional number;</pre>';
$text = 'Text';
echo is_string($text) ? '<pre>g) The variable is a string;</pre>' : '<pre>g) The variable is not a string;</pre>';
$num = null;
echo is_numeric($num) ? '<pre>h) The variable is a number;</pre>' : '<pre>h) The variable is not a number;</pre>';
$num = 1;
echo is_bool($num) ? '<pre>i) The variable is boolean;</pre>' : '<pre>i) The variable is not boolean;</pre>';
$text = 'absd';
echo is_scalar($text) ? '<pre>j) The variable is scalar;</pre>' : '<pre>j) The variable is not scalar;</pre>';
$text = 'text array?';
echo is_array($text) ? '<pre>l) The variable is an array;</pre>' : '<pre>l) The variable is not an array;</pre>';
$text = (object)'Object!';
echo is_object($text) ? '<pre>m) The variable is an object;</pre>' : '<pre>m) The variable is not an object;</pre>';
echo '</div>';

echo '<div style="margin-left: 120px;">';
echo '<p>***Task 1:</p>';
$num1 = rand(-99, 99);
$num2 = rand(-99, 99);
echo '<pre>$num1 + ($num2) = '. ($num1 + $num2). '</pre>';
echo '<pre>$num1 * ($num2) = '. ($num1 * $num2). '</pre>';

echo '<p>***Task 2:</p>';
$num1 = rand(-99, 99);
$num2 = rand(-99, 99);
echo '<pre>($num1)<sup>2</sup> + ($num2)<sup>2</sup> = '. (pow($num1, 2) + pow($num2, 2)). '</pre>';

echo '<p>***Task 3:</p>';
$num1 = rand(-99, 99);
$num2 = rand(-99, 99);
$num3 = rand(-99, 99);
echo '<pre>The arithmetic mean of $num1, $num2, and $num3 = '. (($num1 + $num2 + $num3) / 3). '</pre>';

echo '<p>***Task 4:</p>';
$num1 = rand(-99, 99);
$num2 = rand(-99, 99);
$num3 = rand(-99, 99);
echo '<pre>($num1 + 1) - 2 * ($num3 - 2 * ($num1) + ($num2)) = '. (($num1 + 1) - 2 * ($num3 - 2 * $num1 + $num2)). '</pre>';

echo '<p>***Task 5:</p>';
$num = rand(1, 100);
echo '<pre>$num mod 3 = '. ($num % 3). '</pre>';
echo '<pre>$num mod 5 = '. ($num % 5). '</pre>';
$num = rand(1, 100);
echo '<pre>$num + 30% of $num = '. (0.3 * $num + $num). '</pre>';
echo '<pre>$num + 120% of $num = '. (1.2 * $num + $num). '</pre>';

echo '<p>***Task 6:</p>';
$num1 = rand(1, 100);
$num2 = rand(1, 100);
echo '<pre>40% of $num1 + 84% of $num2 = '. (0.4 * $num1 + 0.84 * $num2). '</pre>';
$num = rand(100, 999);
echo '<pre>The sum of the digits of the number $num = '. (array_sum(str_split($num, 1))). '</pre>';

echo '<p>***Task 7:</p>';
$num = rand(100, 999);
$num = str_split($num, 1);
$num[1] = 0;
echo '<pre>Number '. implode($num). ' is reversed: '. (implode(array_reverse($num))). '</pre>';

echo '<p>***Task 8:</p>';
$num = rand(1, 100);
echo ($num % 2) ? '<pre>Number $num is odd</pre>' : '<pre>Number $num is even</pre>';
echo '</div>';
