<?php

/**
 * @param integer $min
 * @param integer $max
 * @param integer $amount
 * @return array<string>
 */
function createGeneratedNumbers($min = -100, $max = 100, $amount = 10): array
{
    $numbers = [];
    for ($i = 0; $i < $amount; $i++) {
        $numbers[] = random_int($min, $max);
    }

    return $numbers;
}
/*?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/main.css">
    <title>Homework 7</title>
</head>

<body>
    <p>Task 1:</p>
    <!--
            Найти минимальное и максимальное среди 3 чисел
        -->
    <div class="task-result">
        <?php*/
$minAndMaxNumUseExpression = function (array $numbers): array {
    $count = count($numbers);
    $min = $numbers[0];
    $max = $numbers[0];
    for ($i = 1; $i < $count; $i++) {
        if ($min > $numbers[$i]) {
            $min = $numbers[$i];
        } elseif ($max < $numbers[$i]) {
            $max = $numbers[$i];
        }
    }

    return [
        'min' => $min,
        'max' => $max,
    ];
};
$minUseArrow = fn (float $num1, float $num2, float $num3): float => ($num1 <= $num2 && $num1 <= $num3) ? $num1 : (($num2 <= $num1 && $num2 <= $num3) ? $num2 : $num3);
$maxUseArrow = fn (float $num1, float $num2, float $num3): float => ($num1 >= $num2 && $num1 >= $num3) ? $num1 : (($num2 >= $num1 && $num2 >= $num3) ? $num2 : $num3);

$num1 = random_int(-100, 100);
$num2 = random_int(-100, 100);
$num3 = random_int(-100, 100);
$minAndMax = $minAndMaxNumUseExpression([$num1, $num2, $num3]);
echo 'From numbers $num1, $num2, $num3'. ':<br>';
echo 'Using function '. 'expression:<br>';
echo '&nbspMin = '. $minAndMax['min']. '<br>';
echo '&nbspMax = '. $minAndMax['max']. '<br>';
echo 'Using arrow function:<br>';
echo '&nbspMin = '. $minUseArrow($num1, $num2, $num3). '<br>';
echo '&nbspMax = '. $maxUseArrow($num1, $num2, $num3). '<br>';
/*?>
    </div>
    <div>
        <p>Task 2:</p>
        <!--
            Найти площадь
        -->
        <div class="task-result">
            <?php*/
$areaRectangleUseExpression = function (float $height, float $width): float {
    if ($height < 1 || $width < 1) {
        return 0;
    }
    return $height * $width;
};
$areaRectangleUseArrow = fn (float $height, float $width): float => ($height < 1 || $width < 1) ? 0 : $height * $width;

$height = random_int(1, 100);
$width = random_int(1, 100);
echo 'Area of a rectangle with lengths of $height and '. $width. ' parallel sides:<br>';
echo 'Using function expression: '. $areaRectangleUseExpression($height, $width). ' c.u.<br>';
echo 'Using arrow function: '. $areaRectangleUseArrow($height, $width). ' c.u.';
/*?>
        </div>
        <p>Task 3:</p>
        <!--
            Теорема Пифагора
        -->
        <div class="task-result">
            <?php*/
$hypotenuseLengthUseExpression = function (float $cathetus1, float $cathetus2): float {
    if ($cathetus1 < 1 || $cathetus2 < 1) {
        return 0;
    }

    return sqrt($cathetus1 * $cathetus1 + $cathetus2 * $cathetus2);
};
$hypotenuseLengthUseArrow = fn (float $cathetus1, float $cathetus2): float => ($cathetus1 < 1 || $cathetus2 < 1) ? 0 : sqrt(pow($cathetus1, 2) + pow($cathetus2, 2));

$cathetus1 = random_int(1, 100);
$cathetus2 = random_int(1, 100);
echo 'The length of the hypotenuse along the cathetus of length '. $cathetus1. ' and '. $cathetus2. ':<br>';
echo 'Using function expression: '. round($hypotenuseLengthUseExpression($cathetus1, $cathetus2), 2). ' c.u.<br>';
echo 'Using arrow function: '. round($hypotenuseLengthUseArrow($cathetus1, $cathetus2), 2). ' c.u.';
/*?>
        </div>
        <p>Task 4:</p>
        <!--
            Найти периметр
        -->
        <div class="task-result">
            <?php*/
$perimeterUseExpression = function (array $sides): float {
    $result = 0;
    foreach ($sides as $side) {
        if ($side < 1) {
            return 0;
        }
        $result += $side;
    }

    return $result;
};
$perimeterUseArrow = fn (float $side1, float $side2, float $side3): float => ($side1 < 1 || $side2 < 1 || $side3 < 1) ? 0 : $side1 + $side2 + $side3;

$side1 = random_int(1, 100);
$side2 = random_int(1, 100);
$side3 = random_int(1, 100);
echo 'Perimeter with side lengths '. $side1. ', '. $side2. ', '. $side3. ':<br>';
echo 'Using function expression: '. $perimeterUseExpression([$side1, $side2, $side3]). ' c.u.<br>';
echo 'Using function arrow: '. $perimeterUseArrow($side1, $side2, $side3). ' c.u.';
/*?>
        </div>
        <p>Task 5:</p>
        <!--
            Найти дискриминант
        -->
        <div class="task-result">
            <?php*/
$discriminantUseExpression = function (float $a, float $b, float $c): float {
    return -$b + 4 * $a * $c;
};
$discriminantUseArrow = fn (float $a, float $b, float $c): float => - $b + 4 * $a * $c;

$a = random_int(-50, 50);
$b = random_int(-50, 50);
$c = random_int(-50, 50);
echo 'The discriminant of the square trinomial for a = '. $a. ', b = '. $b. ', c = '. $c. ':<br>';
echo 'Using function expression: '. $discriminantUseExpression($a, $b, $c);
echo '<br>Using function arrow: '. $discriminantUseArrow($a, $b, $c);
/*?>
        </div>
        <p>Task 6:</p>
        <!--
            Создать только четные числа до 100
        -->
        <div class="task-result">
            <?php*/
$evenOrOddNums = function (bool $isEven, int $untilNumber = 100): array {
    $nums = [];
    $startNum = $isEven ? 0 : 1;
    for ($i = $startNum; $i <= $untilNumber; $i += 2) {
        $nums[] = $i;
    }

    return $nums;
};


echo 'Array with even numbers up to 100: ['. implode(', ', $evenOrOddNums(true)). ']';
/*?>
        </div>
        <p>Task 7:</p>
        <!--
            Создать нечетные числа до 100
        -->
        <div class="task-result">
            <?php*/
echo 'Array with even numbers up to 100: ['. implode(', ', $evenOrOddNums(false)). ']';
/*?>
        </div>
        <p>Task 8:</p>
        <!--
            Определите, есть ли в массиве повторяющиеся элементы.
        -->
        <div class="task-result">
            <?php*/
$isRepeatingNums = function (array $numbers): bool {
    $count = count($numbers);
    for ($i = 0; $i < $count; $i++) {
        for ($j = $i + 1; $j < $count; $j++) {
            if ($numbers[$i] == $numbers[$j]) {
                return true;
            }
        }
    }

    return false;
};


$nums = createGeneratedNumbers();
echo 'Are there duplicate numbers in array ['. implode(', ', $nums). ']?<br>';
echo 'Answer: ';
echo $isRepeatingNums($nums) ? 'Yes' : 'No';
/*?>
        </div>
        <p>Task 9:</p>
        <!--
            написать функцию сортировки. Функция принимает массив случайных чисел и сортирует их по порядку.
            По дефолту функция сортирует в порядке возрастания. Но если передать в сторой параметр то функция будет сортировать по убыванию.
            sort(arr)
            sort(arr, 'asc')
            sort(arr, 'desc')
        -->
        <div class="task-result">
            <?php*/
$sortArray = function (array $nums, string $order = 'asc'): array {
    $count = count($nums);
    if ($order === 'asc') {
        for ($i = 0; $i < $count; $i++) {
            for ($j = $count - 1; $j > $i; $j--) {
                if ($nums[$i] < $nums[$j]) {
                    $tempElem = $nums[$i];
                    $nums[$i] = $nums[$j];
                    $nums[$j] = $tempElem;
                }
            }
        }
    } elseif ($order === 'desc') {
        for ($i = 0; $i < $count; $i++) {
            for ($j = $count - 1; $j > $i; $j--) {
                if ($nums[$i] > $nums[$j]) {
                    $tempElem = $nums[$i];
                    $nums[$i] = $nums[$j];
                    $nums[$j] = $tempElem;
                }
            }
        }
    }

    return $nums;
};

$nums = createGeneratedNumbers();
echo 'Array ['. implode(', ', $nums). ']<br>';
echo 'Ascending: ['. implode(', ', $sortArray($nums)). ']<br>';
echo 'Descending: ['. implode(', ', $sortArray($nums, 'desc')). ']';
            /*?>
        </div>
</body>

</html>*/
