<?php

require_once 'helpFunctions.php';

/**
 * @param integereger $amount
 * @return void
 */
function printSimpleNumbers($amount): void
{
    $count = 0;
    for ($i = 2; $count < $amount; $i++) {
        if (getIsSimpleNumber($i)) {
            $count++;
            echo $count != $amount ? $i.', ' : $i;
        }
    }
}

/**
 * @param integer[] $numbers
 * @return integereger
 */
function calcEvenCount(array $numbers): int
{
    $count = 0;
    foreach ($numbers as $number) {
        if ($number % 2 === 0) {
            $count++;
        }
    }

    return $count;
}

/**
 * @param integer[] $numbers
 * @param integer[] $amountOfList
 * @return array<int, int>
 */
function calcNumbersAmount(array $numbers, array $amountOfList): array
{
    $numbersAmount = [];
    foreach ($amountOfList as $amountOf) {
        $numbersAmount[$amountOf] = 0;
        foreach ($numbers as $number) {
            if ($number === $amountOf) {
                $numbersAmount[$amountOf]++;
            }
        }
    }

    return $numbersAmount;
}

/**
 * @param integer $cols
 * @param integer $rows
 * @return void
 */
function printTable($cols, $rows): void
{
    echo '<table>';
    for ($i = 0; $i < $cols; $i++) {
        echo '<tr>';
        for ($j = 0; $j < $rows; $j++) {
            echo "<td class='";
            if (($i === 0 || $i === $cols - 1) && ($j === 0 || $j === $rows - 1)) {
                echo 'bg-green';
            } elseif ($j === $i || $cols - $i - 1 === $rows - $j - 1) {
                echo 'bg-red';
            }
            echo "'></td>";
        }
        echo '</tr>';
    }
    echo '</table>';
}

define('SEASONS_MONTHS', [
    'winter' => [1, 2, 12],
    'summer' => [6, 7, 8],
    'spring' => [3, 4, 5],
    'autumn' => [9, 10, 11],
]);


/**
 * @param integer $month
 * @return string
 */
function getSeason($month): string
{
    foreach (SEASONS_MONTHS as $season => $seasonMonths) {
        if (in_array($month, $seasonMonths)) {
            return $season;
        }
    }

    return 'incorrect month number '. $month;
}

/**
 * @param string   $str
 * @param string[] $startWithOr
 * @return boolean
 */
function checkIsStringStartWith($str, array $startWithOr): bool
{
    foreach ($startWithOr as $startWith) {
        if (strpos($str, $startWith) == 0) {
            return true;
        }
    }

    return false;
}

/**
 * @param boolean $test
 * @return string
 */
function getTestUsingIf($test): string
{
    if ($test) {
        return 'Right';
    }

    return 'Wrong';
}

/**
 * @param boolean $test
 * @return string
 */
function getTestUsingTrainer($test): string
{
    return $test ? 'Right' : 'Wrong';
}

define('DAYS_WEEK', [
    'ru' => ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'],
    'en' => ['mon', 'tue', 'wed', 'thu', 'fri', 'set', 'sun'],
]);

/**
 * @param string $lang
 * @return array
 */
function getDaysWeekUsingIf($lang): array
{
    if ($lang == 'ru') {
        return DAYS_WEEK[$lang];
    } elseif ($lang == 'en') {
        return DAYS_WEEK[$lang];
    }
    return [];
}

/**
 * @param string $lang
 * @return array
 */
function getDaysWeekUsingTrainer($lang): array
{
    return $lang == 'ru' ? DAYS_WEEK[$lang] : ($lang == 'en' ? DAYS_WEEK[$lang] : []);
}

/**
 * @param integer $clock
 * @return integer
 */
function getQuarterHourUsingIf($clock): int
{
    $quarterHour = 0;
    if ($clock > 0) {
        for ($minute = 15; $minute <= 60; $minute += 15) {
            if ($clock <= $minute) {
                $quarterHour = $minute / 15;
                break;
            }
        }
    }
    return $quarterHour;
}

/**
 * @param integer $clock
 * @return integer
 */
function getQuarterHourUsingTrainer($clock): int
{
    $quarterHour = 0;
    if ($clock > 0) {
        for ($minute = 15; $minute <= 60; $minute += 15) {
            $quarterHour = ($clock <= $minute) && !$quarterHour ? $minute / 15 : $quarterHour;
        }
    }
    return $quarterHour;
}

/**
 * @param array $arr
 * @return array<mixed>
 */
function getArrayReverseUsingFor(array $arr): array
{
    $arrReverse = [];
    for ($i = getArrayCount($arr) - 1; $i >= 0; $i--) {
        $arrReverse[] = $arr[$i];
    }
    return $arrReverse;
}

/**
 * @param array $arr
 * @return array<mixed>
 */
function getArrayReverseUsingForeach(array $arr): array
{
    $arrReverse = [];
    $lastKey = getArrayCount($arr) - 1;
    foreach ($arr as $key => $value) {
        $arrReverse[] = $arr[$lastKey - $key];
    }
    return $arrReverse;
}

/**
 * @param array $arr
 * @return array<mixed>
 */
function getArrayReverseUsingWhile(array $arr): array
{
    $arrReverse = [];
    $keyElem = getArrayCount($arr) - 1;
    while ($keyElem >= 0) {
        $arrReverse[] = $arr[$keyElem];
        $keyElem--;
    }
    return $arrReverse;
}

/**
 * @param array $arr
 * @return array<mixed>
 */
function getArrayReverseUsingDowhile(array $arr): array
{
    $arrReverse = [];
    $keyElem = getArrayCount($arr) - 1;
    do {
        $arrReverse[] = $arr[$keyElem];
        $keyElem--;
    } while ($keyElem >= 0);
    return $arrReverse;
}

/**
 * @param string $str
 * @return string
 */
function getStringReverseUsingFor($str): string
{
    $strReverse = '';
    for ($i = getStringCount($str) - 1; $i >= 0; $i--) {
        $strReverse .= $str[$i];
    }
    return $strReverse;
}

/**
 * @param string $str
 * @return string
 */
function getStringReverseUsingForeach($str): string
{
    $strReverse = '';
    $arr = getConvertStringToArray($str);
    $lastElem = getArrayCount($arr) - 1;
    foreach ($arr as $key => $value) {
        $strReverse .= $arr[$lastElem - $key];
    }
    return $strReverse;
}

/**
 * @param string $str
 * @return string
 */
function getStringReverseUsingWhile($str): string
{
    $strReverse = '';
    $keyElem = getStringCount($str) - 1;
    while ($keyElem >= 0) {
        $strReverse .= $str[$keyElem];
        $keyElem--;
    }
    return $strReverse;
}

/**
 * @param string $str
 * @return string
 */
function getStringReverseUsingDowhile($str): string
{
    $strReverse = '';
    $keyElem = getStringCount($str) - 1;
    do {
        $strReverse .= $str[$keyElem];
        $keyElem--;
    } while ($keyElem >= 0);
    return $strReverse;
}

/**
 * @param string $str
 * @return string
 */
function getLowercaseStringUsingFor($str): string
{
    $count = getStringCount($str);
    for ($i = 0; $i < $count; $i++) {
        $asciiCode = ord($str[$i]);
        if (($asciiCode > 64 && $asciiCode < 91)) {
            for ($j = 97; $j < 123; $j++) {
                if (($asciiCode + 32) == $j) {
                    $str[$i] = chr($j);
                }
            }
        }
    }
    return $str;
}

/**
 * @param string $str
 * @return string
 */
function getLowercaseStringUsingForeach($str): string
{
    $strLowercase = '';
    $count = getStringCount($str);
    $arr = getConvertStringToArray($str);
    foreach ($arr as $key => $value) {
        $asciiCode = ord($value);
        if (($asciiCode > 64 && $asciiCode < 91)) {
            for ($j = 97; $j < 123; $j++) {
                if (($asciiCode + 32) == $j) {
                    $arr[$key] = chr($j);
                }
            }
        }
        $strLowercase .= $arr[$key];
    }
    return $strLowercase;
}

/**
 * @param string $str
 * @return string
 */
function getLowercaseStringUsingWhile($str): string
{
    $count = getStringCount($str);
    $i = 0;
    while ($i < $count) {
        $asciiCode = ord($str[$i]);
        if (($asciiCode > 64 && $asciiCode < 91)) {
            $j = 97;
            while ($j < 123) {
                if (($asciiCode + 32) == $j) {
                    $str[$i] = chr($j);
                }
                $j++;
            }
        }
        $i++;
    }
    return $str;
}

/**
 * @param string $str
 * @return string
 */
function getLowercaseStringUsingDowhile($str): string
{
    $count = getStringCount($str);
    $i = 0;
    do {
        $asciiCode = ord($str[$i]);
        if (($asciiCode > 64 && $asciiCode < 91)) {
            $j = 97;
            while ($j < 123) {
                if (($asciiCode + 32) == $j) {
                    $str[$i] = chr($j);
                }
                $j++;
            }
        }
        $i++;
    } while ($i < $count);
    return $str;
}

/**
 * @param string $str
 * @return string
 */
function getUppercaseStringUsingFor($str): string
{
    $count = getStringCount($str);
    for ($i = 0; $i < $count; $i++) {
        $asciiCode = ord($str[$i]);
        if (($asciiCode > 96 && $asciiCode < 123)) {
            for ($j = 65; $j < 91; $j++) {
                if (($asciiCode - 32) == $j) {
                    $str[$i] = chr($j);
                }
            }
        }
    }
    return $str;
}

/**
 * @param string $str
 * @return string
 */
function getUppercaseStringUsingForeach($str): string
{
    $strUppercase = '';
    $count = getStringCount($str);
    $arr = getConvertStringToArray($str);
    foreach ($arr as $key => $value) {
        $asciiCode = ord($value);
        if (($asciiCode > 96 && $asciiCode < 123)) {
            for ($j = 65; $j < 91; $j++) {
                if (($asciiCode - 32) == $j) {
                    $arr[$key] = chr($j);
                }
            }
        }
        $strUppercase .= $arr[$key];
    }
    return $strUppercase;
}

/**
 * @param string $str
 * @return string
 */
function getUppercaseStringUsingWhile($str): string
{
    $count = getStringCount($str);
    $i = 0;
    while ($i < $count) {
        $asciiCode = ord($str[$i]);
        if (($asciiCode > 96 && $asciiCode < 123)) {
            $j = 65;
            while ($j < 91) {
                if (($asciiCode - 32) == $j) {
                    $str[$i] = chr($j);
                }
                $j++;
            }
        }
        $i++;
    }
    return $str;
}

/**
 * @param string $str
 * @return string
 */
function getUppercaseStringUsingDowhile($str): string
{
    $count = getStringCount($str);
    $i = 0;
    do {
        $asciiCode = ord($str[$i]);
        if (($asciiCode > 96 && $asciiCode < 123)) {
            $j = 65;
            while ($j < 91) {
                if (($asciiCode - 32) == $j) {
                    $str[$i] = chr($j);
                }
                $j++;
            }
        }
        $i++;
    } while ($i < $count);
    return $str;
}

/**
 * @param array $arr
 * @return array
 */
function getLowercaseArrayUsingFor(array $arr): array
{
    $arrLowercase = [];
    $count = getArrayCount($arr);
    for ($i = 0; $i < $count; $i++) {
        $arrLowercase[] = getLowercaseStringUsingFor($arr[$i]);
    }
    return $arrLowercase;
}

/**
 * @param array $arr
 * @return array
 */
function getLowercaseArrayUsingForeach(array $arr): array
{
    $arrLowercase = [];
    $count = getArrayCount($arr);
    foreach ($arr as $key => $value) {
        $arrLowercase[] = getLowercaseStringUsingFor($arr[$key]);
    }
    return $arrLowercase;
}

/**
 * @param array $arr
 * @return array
 */
function getLowercaseArrayUsingWhile(array $arr): array
{
    $arrLowercase = [];
    $count = getArrayCount($arr);
    $i = 0;
    while ($i < $count) {
        $arrLowercase[] = getLowercaseStringUsingFor($arr[$i]);
        $i++;
    }
    return $arrLowercase;
}

/**
 * @param array $arr
 * @return array
 */
function getLowercaseArrayUsingDowhile(array $arr): array
{
    $arrLowercase = [];
    $count = getArrayCount($arr);
    $i = 0;
    do {
        $arrLowercase[] = getLowercaseStringUsingFor($arr[$i]);
        $i++;
    } while ($i < $count);
    return $arrLowercase;
}

/**
 * @param array $arr
 * @return array
 */
function getUppercaseArrayUsingFor(array $arr): array
{
    $arrUppercase = [];
    $count = getArrayCount($arr);
    for ($i = 0; $i < $count; $i++) {
        $arrUppercase[] = getUppercaseStringUsingFor($arr[$i]);
    }
    return $arrUppercase;
}

/**
 * @param array $arr
 * @return array
 */
function getUppercaseArrayUsingForeach(array $arr): array
{
    $arrUppercase = [];
    $count = getArrayCount($arr);
    foreach ($arr as $key => $value) {
        $arrUppercase[] = getUppercaseStringUsingFor($arr[$key]);
    }
    return $arrUppercase;
}

/**
 * @param array $arr
 * @return array
 */
function getUppercaseArrayUsingWhile(array $arr): array
{
    $arrUppercase = [];
    $count = getArrayCount($arr);
    $i = 0;
    while ($i < $count) {
        $arrUppercase[] = getUppercaseStringUsingFor($arr[$i]);
        $i++;
    }
    return $arrUppercase;
}

/**
 * @param array $arr
 * @return array
 */
function getUppercaseArrayUsingDowhile(array $arr): array
{
    $arrUppercase = [];
    $count = getArrayCount($arr);
    $i = 0;
    do {
        $arrUppercase[] = getUppercaseStringUsingFor($arr[$i]);
        $i++;
    } while ($i < $count);
    return $arrUppercase;
}

/**
 * @param integer $num
 * @return integer
 */
function getNumReverseUsingFor($num): int
{
    $absNum = abs($num);
    $reverseNum = 0;
    for ($i = 1; (int)($absNum / $i) > 0; $i *= 10) {
        $reverseNum += (int)($absNum / $i) % 10;
        $reverseNum *= 10;
    }
    return $num > 0 ? $reverseNum / 10 : -$reverseNum / 10;
}

/**
 * @param integer $num
 * @return integer
 */
function getNumReverseUsingForeach($num): int
{
    $reverseNum = 0;
    $arr = getConvertNumToArrayReverse(abs($num));
    foreach ($arr as $value) {
        $reverseNum += $value;
        $reverseNum *= 10;
    }
    return $num > 0 ? $reverseNum / 10 : -$reverseNum / 10;
}

/**
 * @param integer $num
 * @return integer
 */
function getNumReverseUsingWhile($num): int
{
    $absNum = abs($num);
    $reverseNum = 0;
    $i = 1;
    while ((int)($absNum / $i) > 0) {
        $reverseNum += (int)($absNum / $i) % 10;
        $reverseNum *= 10;
        $i *= 10;
    }
    return $num > 0 ? $reverseNum / 10 : -$reverseNum / 10;
}

/**
 * @param integer $num
 * @return integer
 */
function getNumReverseUsingDowhile($num): int
{
    $absNum = abs($num);
    $reverseNum = 0;
    $i = 1;
    do {
        $reverseNum += (int)($absNum / $i) % 10;
        $reverseNum *= 10;
        $i *= 10;

    } while ((int)($absNum / $i) > 0);
    return $num > 0 ? $reverseNum / 10 : -$reverseNum / 10;
}

/**
 * @param array $arr
 * @return array
 */
function getSortArrayUsingFor(array $arr): array
{
    $count = getArrayCount($arr);
    for ($i = 0; $i < $count; $i++) {
        for ($j = $count - 1; $j > $i; $j--) {
            if ($arr[$i] < $arr[$j]) {
                $tempElem = $arr[$i];
                $arr[$i] = $arr[$j];
                $arr[$j] = $tempElem;
            }
        }
    }
    return $arr;
}

/**
 * @param array $arr
 * @return array
 */
function getSortArrayUsingForeach(array $arr): array
{
    $count = getArrayCount($arr);
    foreach ($arr as $key => $value) {
        for ($j = $count - 1; $j > $key; $j--) {
            if ($arr[$key] < $arr[$j]) {
                $tempElem = $arr[$key];
                $arr[$key] = $arr[$j];
                $arr[$j] = $tempElem;
            }
        }
    }
    return $arr;
}

/**
 * @param array $arr
 * @return array
 */
function getSortArrayUsingWhile(array $arr): array
{
    $count = getArrayCount($arr);
    $i = 0;
    while ($i < $count) {
        $j = $count - 1;
        while ($j > $i) {
            if ($arr[$i] < $arr[$j]) {
                $tempElem = $arr[$i];
                $arr[$i] = $arr[$j];
                $arr[$j] = $tempElem;
            }
            $j--;
        }
        $i++;
    }
    return $arr;
}

/**
 * @param array $arr
 * @return array
 */
function getSortArrayUsingDowhile(array $arr): array
{
    $count = getArrayCount($arr);
    $i = 0;
    do {
        $j = $count - 1;
        do {
            if ($arr[$i] < $arr[$j]) {
                $tempElem = $arr[$i];
                $arr[$i] = $arr[$j];
                $arr[$j] = $tempElem;
            }
            $j--;
        } while ($j > $i);
        $i++;
    } while ($i < $count);
    return $arr;
}
