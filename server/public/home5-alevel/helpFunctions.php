<?php

/**
 * @param integer $number
 * @return boolean
 */
function getIsSimpleNumber($number): bool
{
    $sqrt = sqrt($number);
    $ceilSqrt = ceil($sqrt) == $sqrt ? ++$sqrt : ceil($sqrt);
    for ($i = 2; $i < $ceilSqrt; $i++) {
        if ($number % $i === 0) {
            return false;
        }
    }

    return true;
}

/**
 * @param integer $min
 * @param integer $max
 * @param integer $amount
 * @return integer[]
 */
function getGeneratedNumbers($min = 1, $max = 100000, $amount = 100): array
{
    $numbers = [];
    for ($i = 0; $i < $amount; $i++) {
        $numbers[] = random_int($min, $max);
    }

    return $numbers;
}

/**
 * @param array $arr
 * @return integer
 */
function getArrayCount(array $arr): int
{
    $count = 0;
    foreach ($arr as $value) {
        $count++;
    }
    return $count;
}

/**
 * @param array  $arr
 * @param string $wrapStart
 * @param string $wrapEnd
 * @return void
 */
function printArray(array $arr, $wrapStart = '[', $wrapEnd = ']'): void
{
    $lastKey = getArrayCount($arr) - 1;
    echo $wrapStart;
    for ($i = 0; $i <= $lastKey; $i++) {
        echo $arr[$i];
        if ($i != $lastKey) {
            echo ', ';
        }
    }
    echo $wrapEnd;
}

/**
 * @param integer $num
 * @return array
 */
function getConvertNumToArrayReverse($num): array
{
    $absNum = abs($num);
    if ($num < 0) {
        $arr[] = '-';
    }
    $arr[] = $absNum % 10;
    for ($i = 10; (int)($absNum / $i) > 0; $i *= 10) {
        $arr[] = (int)($absNum / $i) % 10;
    }
    return $arr;
}

/**
 * @param string $str
 * @return array
 */
function getConvertStringToArray($str): array
{
    $arr = [];
    for ($i = 0; $str[$i]; $i++) {
        $arr[] = $str[$i];
    }
    return $arr;
}

/**
 * @param string $str
 * @return integer
 */
function getStringCount($str): int
{
    $count = 0;
    while (isset($str[$count])) {
        $count++;
    }

    return $count;
}

/**
 * @param string  $submissionResult
 * @param boolean $needButton
 * @param string  $textButton
 * @return void
 */
function showMessageSubmissionResult($submissionResult, $needButton = false, $textButton = ''): void
{
    echo "<div id='submitted-info'>";
    echo '<p>'. $submissionResult. '</p>';
    if ($needButton) {
        echo '<a id="button-back-game" href="game.php">'. $textButton. '</a>';
    }
    echo '</div>';
}
