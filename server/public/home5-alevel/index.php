<?php
require_once 'taskFunctions.php';
require_once 'helpFunctions.php';
/*?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/main.css">
        <title>Homework 5</title>
    </head>
    <body>
        <!--
            Давайте попробуем написать игру-угадывалку, состоящую из нескольких файлов.
            Файл index.php у нас будет главным, на нем все и будет происходить.
            В него подключим файл form.php – в нем расположим форму с одним текстовым
            полем и кнопкой «угадать».
            В поле мы вводим цифру и отправляем на index.php.
            Там проверяем что пришли данные из формы и если они пришли, то генерируем
            число от 5 до 8 и сравниваем с пришедшим числом из формы.
            Если число пришло меньше 5 – пишем что число маленькое.
            Если больше 8 – то пишем что число большое.
            Если мы угадали число – выводи на экран надпись «вы угадали» и ссылку на
            пустую форму с надписью «Играть еще».
            Если не угадали, но число в нужном диапазоне – выводим опять форму с надписью «попробуйте еще»
        -->
        <form id="form-game" action="game.php" method="get">
            <input name="start-game" type="submit" value="Let's play!">
        </form>
        <div id="flex">
            <div>
                <p>**Task 1:</p>
                <!--
                    Написать программу, которая выводит простые числа, т.е. делящиеся без
                    остатка только на себя и на 1.
                -->
                <div class="task-result">
                    <?php*/
printSimpleNumbers(10);
/*?>
                </div>
                <p>**Task 2:</p>
                <!--
                    Сгенерируйте 100 раз новое число и выведите на экран количество четных
                    чисел из этих 100.
                -->
                <div class="task-result">
                    <?php*/
echo calcEvenCount(getGeneratedNumbers()). ' out of 100 random numbers are even';
/*?>
                </div>
                <p>**Task 3:</p>
                <!--
                    Сгенерируйте 100 раз число от 1 до 5 и выведите на экран сколько раз
                    сгенерировались эти числа (1, 2, 3, 4 и 5).
                -->
                <div class="task-result">
                    <?php*/
$arr = calcNumbersAmount(getGeneratedNumbers(1, 5), [1, 2, 3, 4, 5]);
echo 'Out of 100 random numbers are repeated:';
foreach ($arr as $key => $value) {
    echo '<br>Number '. $key. ' - '. $value. ' times';
}
/*?>
                </div>
                <p>**Task 4:</p>
                <!--
                    Используя условия и циклы сделать таблицу в 5 колонок и 3 строки
                    (5x3), отметить разными цветами часть ячеек.
                -->
                <div class="task-result">
                    <?php*/
$numCols = 5;
$numRows = 3;
echo 'Generated table('. $numCols. 'x'. $numRows. '): ';
printTable($numCols, $numRows);
/*?>
                </div>
                <p>**Task 5:</p>
                <!--
                    В переменной month лежит какое-то число из интервала от 1 до 12.
                    Определите в какую пору года попадает этот месяц (зима, лето, весна, осень).
                -->
                <div class="task-result">
                    <?php*/
$month = 12;
echo "It's ". getSeason($month);
/*?>
                </div>
                <p>**Task 6:</p>
                <!--
                    Дана строка, состоящая из символов, например, 'abcde'. Проверьте, что первым символом этой
                    строки является буква 'a'. Если это так - выведите 'да', в противном случае выведите 'нет'.
                -->
                <div class="task-result">
                    <?php*/
$str = 'abcde';
$startStr = 'a';
echo "The '". $str. "' line starts with the letter '". $startStr. "'?<br>";
echo !strpos($str, $startStr) ? 'Yes' : 'No';
/*?>
                </div>
                <p>**Task 7:</p>
                <!--
                    Дана строка с цифрами, например, '12345'. Проверьте, что первым символом этой строки
                    является цифра 1, 2 или 3. Если это так - выведите 'да', в противном случае выведите 'нет'.
                -->
                <div class="task-result">
                    <?php*/
$str = '12345';
$startStr = [1, 2, 3];
echo 'Is one of the numbers '. implode(', ', $startStr). " the beginning of line '". $str. "'?<br>";
echo checkIsStringStartWith($str, $startStr) ? 'Yes' : 'No';
/*?>
                </div>
                <p>**Task 8:</p>
                <!--
                    Если переменная test равна true, то выведите 'Верно', иначе выведите 'Неверно'.
                    Проверьте работу скрипта при test, равном true, false.
                    Напишите два варианта скрипта - тернарка и if else.
                -->
                <div class="task-result">
                    <?php*/
$test = true;
echo getTestUsingIf($test). '<br>';
$test = false;
echo getTestUsingTrainer($test);
/*?>
                </div>
                <p>**Task 9:</p>
                <!--
                    Дано Два массива рус и англ ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс']
                    Если переменная lang = ru вывести массив на русском языке, а если en то вывести
                    на английском языке. Сделать через if else и через тернарку.
                -->
                <div class="task-result">
                    <?php*/
$lang = 'ru';
echo 'Days of the week in the language '. $lang. ': '. implode(', ', getDaysWeekUsingIf($lang));
echo '<br>';
$lang = 'en';
echo 'Days of the week in the language '. $lang. ': '. implode(', ', getDaysWeekUsingIf($lang));
/*?>
                </div>
                <p>**Task 10:</p>
                <!--
                    В переменной cloсk лежит число от 0 до 59 – это минуты. Определите в какую четверть
                    часа попадает это число (в первую, вторую, третью или четвертую). тернарка и if else.
                -->
                <div class="task-result">
                    <?php*/
$clock = random_int(0, 59);
echo '$clock minutes belong to the '. getQuarterHourUsingIf($clock). ' quarter of an hour';
echo '<br>';
$clock = random_int(0, 59);
echo $clock. ' minutes belong to the '. getQuarterHourUsingTrainer($clock). ' quarter of an hour';
/*?>
                </div>
            </div>
            <div>
            <!--
                Все задания делаем
                - while
                - do while
                - for
                - foreach
                не используй готовые функции
            -->
            <p>***Task 1:</p>
            <!--
                Дан массив ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
                Развернуть этот массив в обратном направлении
            -->
            <div class="task-result">
                <?php*/
$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
echo 'Array ';
printArray($arr);
echo ' in reverse:<br>';
echo 'Using for: ';
printArray(getArrayReverseUsingFor($arr));
echo '<br>Using foreach: ';
printArray(getArrayReverseUsingForeach($arr));
echo '<br>Using for while: ';
printArray(getArrayReverseUsingWhile($arr));
echo '<br>Using for dowhile: ';
printArray(getArrayReverseUsingDowhile($arr));
/*?>
            </div>
            <p>***Task 2:</p>
            <!--
                Дан массив [44, 12, 11, 7, 1, 99, 43, 5, 69]
                Развернуть этот массив в обратном направлении
            -->
            <div class="task-result">
                <?php*/
$arr = [42, 12, 11, 7, 1, 99, 43, 5, 69];
echo 'Array ';
printArray($arr);
echo ' in reverse:<br>';
echo 'Using for: ';
printArray(getArrayReverseUsingFor($arr));
echo '<br>Using foreach: ';
printArray(getArrayReverseUsingForeach($arr));
echo '<br>Using for while: ';
printArray(getArrayReverseUsingWhile($arr));
echo '<br>Using for dowhile: ';
printArray(getArrayReverseUsingDowhile($arr));
/*?>
            </div>
            <p>***Task 3:</p>
            <!--
                Дана строка let str = 'Hi I am ALex'
                Развенуть строку в обратном направлении.
            -->
            <div class="task-result">
                <?php*/
$str = 'Hi I am Alex';
echo "String '". $str. "' in reverse:<br>";
echo "Using for: '". getStringReverseUsingFor($str). "'<br>";
echo "Using foreach: '". getStringReverseUsingForeach($str). "'<br>";
echo "Using while: '". getStringReverseUsingWhile($str). "'<br>";
echo "Using dowhile: '". getStringReverseUsingDowhile($str). "'";
/*?>
            </div>
            <p>***Task 4:</p>
            <!--
                Дана строка. Готовую функцию toUpperCase() or tolowercase()
                let str = 'Hi I am ALex' сделать ее с с маленьких букв
            -->
            <div class="task-result">
                <?php*/
echo "String '". $str. "' in lowercase:<br>";
echo "Using for: '". getLowercaseStringUsingFor($str). "'<br>";
echo "Using foreach: '". getLowercaseStringUsingForeach($str). "'<br>";
echo "Using while: '". getLowercaseStringUsingWhile($str). "'<br>";
echo "Using dowhile: '". getLowercaseStringUsingDowhile($str). "'";
/*?>
            </div>
            <p>***Task 5:</p>
            <!--
                Дана строка let str = 'Hi I am ALex'
                Сделать все буквы большие
            -->
            <div class="task-result">
                <?php*/
echo "String '". $str. "' in uppercase:<br>";
echo "Using for: '". getUppercaseStringUsingFor($str). "'<br>";
echo "Using foreach: '". getUppercaseStringUsingForeach($str). "'<br>";
echo "Using while: '". getUppercaseStringUsingWhile($str). "'<br>";
echo "Using dowhile: '". getUppercaseStringUsingDowhile($str). "'";
/*?>
            </div>
            <p>***Task 6:</p>
            <!--
                Дана строка let str = 'Hi I am ALex'
                Развернуть ее в обратном направлении
            -->
            <div class="task-result">
                <?php*/
echo "String '". $str. "' in reverse:<br>";
echo "Using for: '". getStringReverseUsingFor($str). "'<br>";
echo "Using foreach: '". getStringReverseUsingForeach($str). "'<br>";
echo "Using while: '". getStringReverseUsingWhile($str). "'<br>";
echo "Using dowhile: '". getStringReverseUsingDowhile($str). "'";
/*?>
            </div>
            <p>***Task 7:</p>
            <!--
                Дан массив ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
                Сделать все буквы с маленькой
            -->
            <div class="task-result">
                <?php*/
$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
echo 'Array ';
printArray($arr);
echo ' in lowercase:<br>';
echo 'Using for: ';
printArray(getLowercaseArrayUsingFor($arr));
echo '<br>Using foreach: ';
printArray(getLowercaseArrayUsingForeach($arr));
echo '<br>Using for while: ';
printArray(getLowercaseArrayUsingWhile($arr));
echo '<br>Using for dowhile: ';
printArray(getLowercaseArrayUsingDowhile($arr));
/*?>
            </div>
            <p>***Task 8:</p>
            <!--
                Дан массив ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
                Сделать все буквы с большой
            -->
            <div class="task-result">
                <?php*/
echo 'Array ';
printArray($arr);
echo ' in uppercase:<br>';
echo 'Using for: ';
printArray(getUppercaseArrayUsingFor($arr));
echo '<br>Using foreach: ';
printArray(getUppercaseArrayUsingForeach($arr));
echo '<br>Using for while: ';
printArray(getUppercaseArrayUsingWhile($arr));
echo '<br>Using for dowhile: ';
printArray(getUppercaseArrayUsingDowhile($arr));
/*?>
            </div>
            <p>***Task 9:</p>
            <!--
                Дано число let num = 1234678
                Развернуть ее в обратном направлении
            -->
            <div class="task-result">
                <?php*/
$num = 12345678;
echo 'Number '. $num. ' in reverse:<br>';
echo 'Using for: '. getNumReverseUsingFor($num);
echo '<br>Using foreach: '. getNumReverseUsingForeach($num);
echo '<br>Using for while: '. getNumReverseUsingWhile($num);
echo '<br>Using for dowhile: '. getNumReverseUsingDowhile($num);
/*?>
            </div>
            <p>***Task 10:</p>
            <!--
                Дан массив [44, 12, 11, 7, 1, 99, 43, 5, 69]
                Отсортируй его в порядке убывания
            -->
            <div class="task-result">
                <?php*/
$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
echo 'Array ';
printArray($arr);
echo ' in descending order:<br>';
echo 'Using for: ';
printArray(getSortArrayUsingFor($arr));
echo '<br>Using foreach: ';
printArray(getSortArrayUsingForeach($arr));
echo '<br>Using for while: ';
printArray(getSortArrayUsingWhile($arr));
echo '<br>Using for dowhile: ';
printArray(getSortArrayUsingDowhile($arr));
                /*?>
            </div>
        </div>
    </body>
</html>
