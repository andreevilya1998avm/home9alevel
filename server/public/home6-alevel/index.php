<?php

define('COUNTRIES', [
    ['Turkey', 0],
    ['Egypt', 10],
    ['Italy', 12]
]);

/**
 * @param integer $min
 * @param integer $max
 * @param integer $amount
 * @return int[]
 */
function createGeneratedNumbers($min = -100, $max = 100, $amount = 10): array
{
    $numbers = [];
    for ($i = 0; $i < $amount; $i++) {
        $numbers[] = random_int($min, $max);
    }

    return $numbers;
}

/**
 * @param array<int|float|string> $arr
 * @param string                  $wrapStart
 * @param string                  $wrapEnd
 * @return void
 */
function printArray(array $arr, $wrapStart = '[', $wrapEnd = ']'): void
{
    $lastKey = count($arr) - 1;
    echo $wrapStart;
    for ($i = 0; $i <= $lastKey; $i++) {
        echo $arr[$i];
        if ($i != $lastKey) {
            echo ', ';
        }
    }
    echo $wrapEnd;
}

/**
 * @param string        $submissionResult
 * @param array<string> $resultParams
 * @return void
 */
function printMessageSubmissionResult($submissionResult, array $resultParams = []): void
{
    echo '<div class="submitted-info">';
    echo '<p>'. $submissionResult. '</p>';
    foreach ($resultParams as $value) {
        echo '<div class="tub">'. $value. '</div>';
    }
    echo '</div>';
}

/**
 * @param array<int|float> $numbers
 * @return integer
 */
function calcMultipliPositiveNumsWithEvenArrayIndexes(array $numbers): int
{
    $result = 1;
    $countNums = count($numbers);
    $isMultipli = false;
    for ($i = 0; $i < $countNums; $i += 2) {
        if ($numbers[$i] > 0) {
            $result *= $numbers[$i];
            $isMultipli = true;
        }
    }

    return $isMultipli ? $result : 0;
}

/**
 * @param array<int|float> $numbers
 * @return array<int|float>
 */
function getElemsWithOddArrayIndexes(array $numbers): array
{
    $resultNums = [];
    $countNums = count($numbers);
    for ($i = 1; $i < $countNums; $i += 2) {
        if ($numbers[$i] > 0) {
            $resultNums[] = $numbers[$i];
        }
    }

    return $resultNums;
}

/**
 * @param array<int|float> $numbers
 * @return float
 */
function calcSumNumbers(array $numbers): float
{
    $result = 0;
    foreach ($numbers as $num) {
        $result += $num;
    }

    return $result;
}

/**
 * @param array<int|float> $numbers
 * @return float
 */
function calcMultipli(array $numbers): float
{
    $result = 1;
    $isMultipli = false;
    foreach ($numbers as $num) {
        $result *= $num;
        $isMultipli = true;
    }

    return $isMultipli ? $result : 0;
}

/**
 * @param array<int|float> $numbers
 * @return float
 */
function calcSumSquares(array $numbers): float
{
    $result = 0;
    foreach ($numbers as $num) {
        $result += $num * $num;
    }

    return $result;
}

/**
 * @param array<int|float> $numbers
 * @return float
 */
function calcArithmeticMean(array $numbers): float
{
    $count = count($numbers);
    return $count ? calcSumNumbers($numbers) / $count : 0;
}

/**
 * @param float $num
 * @param float $percent
 * @return float
 */
function calcIncreaseNumByPercentage($num, $percent): float
{
    return $num >= 0 && $percent >= 0 ? $num + $num * ($percent / 100) : 0;
}

/**
 * @param string  $country
 * @param integer $numDays
 * @param boolean $isDiscount
 * @return float
 */
function calcHolidayCost($country, $numDays, $isDiscount): float
{
    $cost = $numDays * 400;
    if ($isDiscount) {
        $cost -= $cost * 0.005;
    }
    foreach (COUNTRIES as $key => $value) {
        if ($country == $key) {
            $cost += $cost * (COUNTRIES[$key][1] / 100);
            break;
        }
    }

    return $cost;
}

/**
 * @return void
 */
function printFormCostHolidaySumissionResult(): void
{
    $country = $_POST['holidayCountry'];
    $numDays = $_POST['numHolidayDays'];
    $isDiscount = $_POST['isHolidayDiscount'] ? true : false;
    $errors = [];

    if (empty($country) && $country !== '0') {
        $errors[] = 'The field "Country of holiday" must be filled!';
    } elseif (!array_key_exists($country, COUNTRIES)) {
        $errors[] = 'The entered country is not served!';
    }

    if (empty($numDays) && $numDays !== '0') {
        $errors[] = 'The field "Number of holiday days" must be filled!';
    } elseif ($numDays < 1) {
        $errors[] = 'The entered number of days cannot be less than 1!';
    }

    if (count($errors)) {
        printMessageSubmissionResult(implode('<br>', $errors));
    } else {
        $haveDiscount = $isDiscount ? 'yes' : 'no';
        printMessageSubmissionResult(
            'The data has been sent successfully and the result has been calculated!',
            [
                'Country: '. COUNTRIES[$country][0],
                'Number of days: '. $numDays,
                'Discount: '. $haveDiscount,
                'Calculate cost: '. calcHolidayCost($country, $numDays, $isDiscount)
            ]
        );
    }
}

/**
 * @return void
 */
function printFormRegistrSumissionResult(): void
{
    $name = $_POST['registrName'];
    $password = $_POST['registrPassword'];
    $email = $_POST['registrEmail'];
    $errors = [];

    if (empty($name)) {
        $errors[] = 'The field "Your name" must be filled!';
    }

    if (empty($password)) {
        $errors[] = 'The field "Your password" must be filled!';
    }

    if (empty($email)) {
        $errors[] = 'The field "Your email" must be filled!';
    }

    if (count($errors)) {
        printMessageSubmissionResult(implode('<br>', $errors));
    } else {
        printMessageSubmissionResult('Registration completed successfully!', [
            'Your name: '. $name,
            'Your password: '. $password,
            'Your email: '. $email,
        ]);
    }
}

/**
 * @return void
 */
function printFormRepeatPhraseSumissionResult(): void
{
    $numRepetitions = $_POST['numRepetitions'];
    if (empty($numRepetitions) && $numRepetitions !== '0') {
        printMessageSubmissionResult('The field "Number of repetitions" must be filled!');
    } elseif ($numRepetitions < 1) {
        printMessageSubmissionResult('The number of repetitions cannot be less than 1!');
    } elseif ($numRepetitions > 1000) {
        printMessageSubmissionResult('Too large number of repetitions!');
    } else {
        $repetitions = '';
        $numRepetitions--;
        for ($i = 0; $i <= $numRepetitions; $i++) {
            $repetitions .= 'Silence is golden!';
            if ($i != $numRepetitions) {
                $repetitions .= '<br>';
            }
        }
        printMessageSubmissionResult($repetitions);
    }
}

/**
 * @param integer $amount
 * @return int[]
 */
function createArrayWithBinaryNums($amount): array
{
    $arr = [];
    for ($i = 0; $i < $amount; $i++) {
        $arr[] = $i % 2 ? 1 : 0;
    }

    return $arr;
}

/**
 * @param array<int|float> $numbers
 * @return boolean
 */
function checkRepeatingNums(array $numbers): bool
{
    $count = count($numbers);
    for ($i = 0; $i < $count; $i++) {
        for ($j = $i + 1; $j < $count; $j++) {
            if ($numbers[$i] == $numbers[$j]) {
                return true;
            }
        }
    }

    return false;
}

/**
 * @param array<int|float> $numbers
 * @return float
 */
function getMinNum(array $numbers): float
{
    $count = count($numbers);
    $min = $numbers[0];
    for ($i = 1; $i < $count; $i++) {
        if ($min > $numbers[$i]) {
            $min = $numbers[$i];
        }
    }

    return $min;
}

/**
 * @param array<int|float> $numbers
 * @return float
 */
function getMaxNum(array $numbers): float
{
    $count = count($numbers);
    $max = $numbers[0];
    for ($i = 1; $i < $count; $i++) {
        if ($max < $numbers[$i]) {
            $max = $numbers[$i];
        }
    }

    return $max;
}

/**
 * @param float $side1Length
 * @param float $side2Length
 * @return float
 */
function calcAreaRectangle($side1Length, $side2Length): float
{
    if ($side1Length < 1 || $side2Length < 1) {
        return 0;
    }
    return $side1Length * $side2Length;
}

/**
 * @param float $cathetus1Length
 * @param float $cathetus2Length
 * @return float
 */
function calcHypotenuseLength($cathetus1Length, $cathetus2Length): float
{
    if ($cathetus1Length < 1 || $cathetus2Length < 1) {
        return 0;
    }

    return sqrt($cathetus1Length * $cathetus1Length + $cathetus2Length * $cathetus2Length);
}

/**
 * @param float $a
 * @param float $b
 * @param float $c
 * @return float
 */
function calcDiscriminant($a, $b, $c): float
{
    return -$b + 4 * $a * $c;
}

/**
 * @param boolean $isEven
 * @param integer $untilNumber
 * @return int[]
 */
function createEvenOrUnevenNumbers($isEven, $untilNumber = 100): array
{
    $arr = [];
    $startNum = $isEven ? 0 : 1;
    for ($i = $startNum; $i <= $untilNumber; $i += 2) {
        $arr[] = $i;
    }

    return $arr;
}

/**
 * @param array<int|float> $arr
 * @param string           $order
 * @return array<int|float>
 */
function getSortArray(array $arr, $order = 'asc'): array
{
    $count = count($arr);
    if ($order == 'asc') {
        for ($i = 0; $i < $count; $i++) {
            for ($j = $count - 1; $j > $i; $j--) {
                if ($arr[$i] < $arr[$j]) {
                    $tempElem = $arr[$i];
                    $arr[$i] = $arr[$j];
                    $arr[$j] = $tempElem;
                }
            }
        }
    } elseif ($order == 'desc') {
        for ($i = 0; $i < $count; $i++) {
            for ($j = $count - 1; $j > $i; $j--) {
                if ($arr[$i] > $arr[$j]) {
                    $tempElem = $arr[$i];
                    $arr[$i] = $arr[$j];
                    $arr[$j] = $tempElem;
                }
            }
        }
    }

    return $arr;
}

/**
 * @param float   $number
 * @param integer $power
 * @return float
 */
function calcNumberPower($number, $power): float
{
    $numPower = 1;
    $absPower = abs($power);
    for ($i = 0; $i < $absPower; $i++) {
        $numPower *= $number;
    }

    return $power < 0 ? 1 / $numPower : $numPower;
}

/**
 * @param array<int|float> $numbers
 * @param float            $searchNumber
 * @return boolean
 */
function checkExistsNumber(array $numbers, $searchNumber): bool
{
    foreach ($numbers as $number) {
        if ($number == $searchNumber) {
            return true;
        }
    }

    return false;
}

/*?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/main.css">
        <title>Homework 6</title>
    </head>
    <body>
        <b class="task-caption">Task 1:</b>
        <!--
            Вам нужно создать массив и заполнить его случайными числами от 1 до 100 (ф-я rand).
            Далее, вычислить произведение тех элементов, которые больше нуля и у которых четные индексы.
            После вывести на экран элементы, которые больше нуля и у которых нечетный индекс.
        -->
        <div class="task-result line-height">
            <?php*/
$numbers = createGeneratedNumbers();
echo 'Generated array: ';
printArray($numbers);
echo '<br>The result of multiplying positive numbers with even indexes: ';
echo calcMultipliPositiveNumsWithEvenArrayIndexes($numbers);
echo '<br>Positive numbers with odd indexes: ';
printArray(getElemsWithOddArrayIndexes($numbers));
/*?>
        </div>
        <b class="task-caption">Task 2:</b>
        <!--
            Даны два числа. Найти их сумму и произведение.
            Даны два числа. Найдите сумму их квадратов.
        -->
        <div class="task-result line-height">
            <?php*/
$num1 = random_int(-100, 100);
$num2 = random_int(-100, 100);
echo 'Generated numbers: '. $num1. ' and '. $num2. '<br>';
echo '('. $num1. ') + ('. $num2. ') = '. calcSumNumbers([$num1, $num2]). '<br>';
echo '('. $num1. ') * ('. $num2. ') = '. calcMultipli([$num1, $num2]);
echo '<div class="fix-sup">('. $num1. ')<sup>2</sup> + ('. $num2. ')<sup>2</sup> = '. calcSumSquares([$num1, $num2]). '</div>';

/*?>
        </div>
        <b class="task-caption">Task 3:</b>
        <!--
            Даны три числа. Найдите их среднее арифметическое.
        -->
        <div class="task-result line-height">
            <?php*/
$num1 = random_int(-100, 100);
$num2 = random_int(-100, 100);
$num3 = random_int(-100, 100);
echo 'The arithmetic mean of the numbers '. $num1. ', '. $num2. ', '. $num3. ': '. round(calcArithmeticMean([$num1, $num2, $num3]), 2). '<br>';
/*?>
        </div>
        <b class="task-caption">Task 4:</b>
        <!--
            Дано число. Увеличьте его на 30%, на 120%.
        -->
        <div class="task-result line-height">
            <?php*/
$num1 = random_int(1, 100);
$percent = random_int(1, 100);
echo 'Number '. $num1. ' increased by '. $percent. '%: '. round(calcIncreaseNumByPercentage($num1, $percent), 2). '<br>';
/*?>
        </div>
        <b class="task-caption">Task 5:</b>
        <!--
            Пользователь выбирает из выпадающего списка страну (Турция, Египет или Италия),
            вводит количество дней для отдыха и указывает, есть ли у него скидка (чекбокс).
            Вывести стоимость отдыха, которая вычисляется как произведение количества дней на 400.
            Далее это число увеличивается на 10%, если выбран Египет, и на 12%, если выбрана Италия.
            И далее это число уменьшается на 5%, если указана скидка.
        -->
        <?php*/
include 'forms/costHoliday.phtml';

if (!is_null($_POST['costHolidayForm'])) {
    printFormCostHolidaySumissionResult();
}
/*?>
        <b class="task-caption">Task 6:</b>
        <!--
            Пользователь вводит свой имя, пароль, email. Если вся информация указана, то показать эти данные
            после фразы 'Регистрация прошла успешно', иначе сообщить какое из полей оказалось не заполненным.
        -->
        <?php*/
include 'forms/registration.html';

if (!is_null($_POST['registrForm'])) {
    printFormRegistrSumissionResult();
}
/*?>
                <b class="task-caption">Task 7:</b>
        <!--
            Выведите на экран n раз фразу "Silence is golden". Число n вводит пользователь на форме.
            Если n некорректно, вывести фразу "Bad n".
        -->
        <?php*/
include 'forms/repeatPhrase.html';

if (!is_null($_POST['repeatPhraseForm'])) {
    printFormRepeatPhraseSumissionResult();
}
/*?>
        <b class="task-caption">Task 8:</b>
        <!--
            Заполнить массив длины n нулями и единицами, при этом данные значения чередуются, начиная с нуля.
        -->
        <div class="task-result line-height">
            <?php*/
$amount = 50;
echo 'An array of $amount elements filled with the numbers 0 and 1: ';
printArray(createArrayWithBinaryNums($amount));
/*?>
        </div>
        <b class="task-caption">Task 9:</b>
        <!--
            Определите, есть ли в массиве повторяющиеся элементы.
        -->
        <div class="task-result line-height">
            <?php*/
$arr = createGeneratedNumbers();
echo 'Are there duplicate numbers in array ';
printArray($arr);
echo '?<br>Answer: ';
echo checkRepeatingNums($arr) ? 'Yes' : 'No';
/*?>
        </div>
        <b class="task-caption">Task 10:</b>
        <!--
            Найти минимальное и максимальное среди 3 чисел
        -->
        <div class="task-result line-height">
            <?php*/
$num1 = random_int(-100, 100);
$num2 = random_int(-100, 100);
$num3 = random_int(-100, 100);
echo 'From numbers '. $num1. ', '. $num2. ', '. $num3. ':<br>';
echo 'Min = '. getMinNum([$num1, $num2, $num3]). '<br>';
echo 'Max = '. getMaxNum([$num1, $num2, $num3]). '<br>';
/*?>
        </div>
        <b class="task-caption">Task 11:</b>
        <!--
            Найти площадь
        -->
        <div class="task-result line-height">
            <?php*/
$side1Length = random_int(1, 100);
$side2Length = random_int(1, 100);
echo 'Area of a rectangle with lengths of '. $side1Length. ' and '. $side2Length. ' parallel sides: ';
echo calcAreaRectangle($side1Length, $side2Length). ' c.u.';
/*?>
        </div>
        <b class="task-caption">Task 12:</b>
        <!--
            Теорема Пифагора
        -->
        <div class="task-result line-height">
            <?php*/
$cathetus1Length = random_int(1, 100);
$cathetus2Length = random_int(1, 100);
echo 'The length of the hypotenuse along the cathetus of length '. $cathetus1Length. ' and '. $cathetus2Length. ': ';
echo round(calcHypotenuseLength($cathetus1Length, $cathetus2Length), 2). ' c.u.';
/*?>
        </div>
        <b class="task-caption">Task 13:</b>
        <!--
            Найти периметр
        -->
        <div class="task-result line-height">
            <?php*/
$side1Length = random_int(1, 100);
$side2Length = random_int(1, 100);
$side3Length = random_int(1, 100);
echo 'Perimeter with side lengths '. $side1Length. ', '. $side2Length. ', '. $side3Length. ': ';
echo calcSumNumbers([$side1Length, $side2Length, $side3Length]). ' c.u.';
/*?>
        </div>
        <b class="task-caption">Task 14:</b>
        <!--
            Найти дискриминант
        -->
        <div class="task-result line-height">
            <?php*/
$a = random_int(-50, 50);
$b = random_int(-50, 50);
$c = random_int(-50, 50);
echo 'The discriminant of the square trinomial for a = '. $a. ', b = '. $b. ', c = '. $c. ': '. calcDiscriminant($a, $b, $c);
/*?>
        </div>
        <b class="task-caption">Task 15:</b>
        <!--
            Создать только четные числа до 100
        -->
        <div class="task-result line-height">
            <?php*/
echo 'Array with even numbers up to 100: ';
printArray(createEvenOrUnevenNumbers(true));
/*?>
        </div>
        <b class="task-caption">Task 16:</b>
        <!--
            Создать не четные числа до 100
        -->
        <div class="task-result line-height">
            <?php*/
echo 'Array with odd numbers up to 100: ';
printArray(createEvenOrUnevenNumbers(false));
/*?>
        </div>
        <b class="task-caption">Task 17:</b>
        <!--
            Создать функцию по нахождению числа в степени
        -->
        <div class="task-result line-height">
            <?php*/
$num1 = random_int(-20, 20);
$power = random_int(-10, 10);
echo 'Number '. $num1. ' to the power of $power: '. calcNumberPower($num1, $power);
/*?>
        </div>
        <b class="task-caption">Task 18:</b>
        <!--
            написать функцию сортировки. Функция принимает массив случайных чисел и сортирует их по порядку.
            По дефолту функция сортирует в порядке возрастания. Но если передать в сторой параметр то функция будет сортировать по убыванию.
            sort(arr)
            sort(arr, 'asc')
            sort(arr, 'desc')
        -->
        <div class="task-result line-height">
            <?php*/
$arr = createGeneratedNumbers();
echo 'Array ';
printArray($arr);
echo '<br>Ascending: ';
printArray(getSortArray($arr));
echo '<br>Descending: ';
printArray(getSortArray($arr, 'desc'));
/*?>
        </div>
        <b class="task-caption">Task 19:</b>
        <!--
            написать функцию поиска в массиве. функция будет принимать два параметра.
            Первый массив, второй поисковое число. search(arr, find)
        -->
        <div class="task-result line-height">
            <?php*/
$arr = createGeneratedNumbers();
$num1 = random_int(-100, 100);
echo 'Is there a number '. $num1. ' in the array ';
printArray($arr, '[', ']?<br>');
echo checkExistsNumber($arr, $num1) ? 'Yes' : 'No';
            /*?>
        </div>
    </body>
</html>
